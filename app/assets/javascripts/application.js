//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require_tree .
//

//

$(document).ready(function() {

  // ALERTS
  $(".flash-message").click(function() {
    $(this).slideUp("slow");
  });

});
